package ro.sd.a2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import ro.sd.a2.service.repository.UserRepository;

@RunWith(SpringRunner.class)
public class MockBeanIntegrationTest {

    @MockBean
    UserRepository mockRepository;

    @Autowired
    ApplicationContext applicationContext;

    @Test
    public void test(){
        Mockito.when(mockRepository.count()).thenReturn(1L);
        UserRepository userRepository=applicationContext.getBean(UserRepository.class);
        long count=userRepository.count();
        Assert.assertEquals(1,count);
        Mockito.verify(mockRepository).count();
    }

}
