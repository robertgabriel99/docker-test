package ro.sd.a2;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import ro.sd.a2.service.NameService;

@Profile("test")
@Configuration
public class NameServiceTestConfiguration {

    @Bean
    @Primary
    public NameService nameService(){
        return Mockito.mock(NameService.class);
    }

}
