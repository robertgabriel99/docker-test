package ro.sd.a2;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ro.sd.a2.service.NameService;
import ro.sd.a2.service.UserService;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Assignment2SdApplication.class)
@WebAppConfiguration
public class UserServiceUnitTest {

    @Autowired
    private UserService userService;

    @Autowired
    private NameService nameService;

    @Test
    public void whenUserIdIsProvided_thenRetrievedNameIsCorrect() {
        Mockito.when(nameService.getUserName("SomeId")).thenReturn("Mock user name");
        String testName = userService.getByName("SomeId");
        Assert.assertEquals("Mock user name", testName);
    }

    @Test
    public void mockitoMockTest(){
        UserService userService=Mockito.mock(UserService.class);
        Mockito.when(userService.getByName("nume")).thenReturn("Mock user name");
        String name=userService.getByName("nume");
        Assert.assertEquals("Mock user name",name);
        Mockito.verify(userService).getByName("nume");
    }

}
