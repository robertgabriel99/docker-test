package ro.sd.a2;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ro.sd.a2.controller.AdminHomeController;
import ro.sd.a2.service.UserService;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;


public class GreetControllerIntegrationTest {

    @InjectMocks
    private AdminHomeController adminHomeController = new AdminHomeController();

    @Mock
    private UserService userService;


    @Autowired
    private MockMvc mvc;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);

        this.mvc= MockMvcBuilders.standaloneSetup(adminHomeController).build();
    }

    @Test
    public void test() throws Exception {
        when(userService.greet()).thenReturn("hello, mock");
        this.mvc.perform(get("/home")).andDo(print());
    }

}
