package ro.sd.a2.message;

public class FlightBookingMessages {

    public static String BOOKING_SUCCESSFUL(String userEmail, String flight){
        return new StringBuilder().append("Booking successful made by user ").append(userEmail)
                .append(" for flight ").append(flight).toString();
    }

    public static String BOOKING_REMOVED(String userEmail,String flight){
        return new StringBuilder().append("Booking removed by user ").append(userEmail)
                .append(" for flight ").append(flight).toString();
    }

}
