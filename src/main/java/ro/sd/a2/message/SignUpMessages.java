package ro.sd.a2.message;

public class SignUpMessages {

    public static final String SIGN_UP_ATTEMPTED="Sign up attempted!";
    public static final String SIGN_UP_SUCCESSFUL(String email){
        return new StringBuilder().append("Sign up successful for new user with email ").append(email).toString();
    }

}
