package ro.sd.a2.message;

public class FlightMessages {

    public static String INSERT_SUCCESSFUL(String flight){
        return new StringBuilder().append("New flight ").append(flight).append(" added!").toString();
    }

    public static String REMOVE_SUCCESSFUL(String flight){
        return new StringBuilder().append("Flight ").append(flight).append(" removed!").toString();
    }

    public static String UPDATE_SUCCESSFUL(String flight){
        return new StringBuilder().append("Update successful for ").append(flight).toString();
    }

}
