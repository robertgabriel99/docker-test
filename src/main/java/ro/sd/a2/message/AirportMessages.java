package ro.sd.a2.message;

public class AirportMessages {

    public static String INSERT_ATTEMPTED="Attempt to insert an airport!";
    public static String INSERT_SUCCESSFUL(String name){
        return new StringBuilder().append("Airport ").append(name).append(" inserted!").toString();
    }
    public static String ALL_AIRPORTS_RETRIEVED="All airports retrieved!";
    public static String REMOVE_ATTEMPTED="Attempt to delete an airport!";
    public static String REMOVE_SUCCESSFULL(String name){
        return new StringBuilder().append("Airport ").append(name).append(" deleted!").toString();
    }
    public static String UPDATE_ATTEMPTED="Attemt to update airport!";
    public static String UPDATE_SUCCESSFUL="Update successful!";

}
