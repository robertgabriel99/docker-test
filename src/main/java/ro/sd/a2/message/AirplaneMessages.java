package ro.sd.a2.message;

public class AirplaneMessages {

    public static String INSERT_ATTEMPTED="Insert attempted for a new airplane!";
    public static String NEW_AIRPLANE_ADDED(String name,int capacity){
        return new StringBuilder().append("New airplane added with name: ").append(name).append(" and capacity: ").append(capacity).toString();
    }
    public static String ALL_AIRPLANES_RETRIEVED="All airplanes retrieved!";
    public static String AIRPLANE_DELETED(String name){
        return new StringBuilder().append("Airplane ").append(name).append(" deleted!").toString();
    }
    public static String DELETE_ATTEMTED= "Delete attempted!";
    public static String UPDATE_NAME_ATTEMPTED="Update name attempt!";
    public static String UPDATE_CAPACITY_ATTEMPTED="Update capacity attempt!";
    public static String UPDATE_SUCCESSFULL(String name){
        return new StringBuilder().append("Update successful for airplane ").append(name).toString();
    }

}
