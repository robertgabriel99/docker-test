package ro.sd.a2.message;

public class LoginMessages {

    public static String LOGIN_ATTEMPTED="Login attempted!";
    public static String LOGIN_SUCCESSFULL(String email){
        return new StringBuilder().append("Login successful for user with email ").append(email).toString();
    }

}
