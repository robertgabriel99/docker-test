package ro.sd.a2.exception;

public class AirplaneExceptions {

    public static String EMPTY_NAME="No name inserted!";
    public static String NO_CAPACITY="The airplane has 0 capacity! Please chesck again the configuration!";
    public static String NO_AIRPLANE_FOUND="No airplane found!";

}
