package ro.sd.a2.exception;

public class FlightExceptions {

    public static String FLIGHT_NULL="Flight is null!";

    public static String INSERT_FAILED="New flight insertion failed!";

    public static String NO_FLIGHT_FOUND="No flight found!";

    public static String NO_AIRPLANE_SELECTED="No airplane selected!";

    public static String UPDATE_FAILED="Update failed";

    public static String REMOVE_FAILED="Remove failed!";

    public static String NEW_PRICE_ZERO="New price is zero!";

    public static String WRONG_DATES="The departure time is after the arrival time!";

}
