package ro.sd.a2.exception;

public class AirportExceptions {

    public static String NULL_OBJECT_INSERT="New airport is null!";
    public static String NO_AIRPORTS_RETRIEVED="No airports found!";
    public static String EMPTY_NAME="Airport's name is empty!";
    public static String REMOVE_FAILED="Remove failed!";
    public static String UPDATE_FAILED="Update failed!";
    public static String INSERT_FAILED="Insert failed!";

}
