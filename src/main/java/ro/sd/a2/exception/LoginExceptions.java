package ro.sd.a2.exception;

public class LoginExceptions {

    public static String invalidEmailLogin(String email){
        return "No user with email "+email;
    }
    public static String noEmailInserted="No email inserted!";
    public static String noPasswordInserted="No password inserted!";
    public static String invalidPassword="Invalid password!";

}
