package ro.sd.a2.exception;

public class SignUpExceptions {

    public static final String PASSWORD_TOO_SHORT="Password must have minimum 6 characters!";
    public static final String INVALID_EMAIL_FORMAT(String email){
        return new StringBuilder().append("Invalid format for email! ").append("Email inserted: ").append(email).toString();
    }
    public static final String EMAIL_EMPTY="Email field is empty!";
    public static final String PASSWORD_EMPTY="Password field is empty!";
    public static final String NAME_EMPTY="Name field is empty!";

}
