package ro.sd.a2.exception;

public class FlightBookingExceptions {

    public static String NO_SEAT_SELECTED="No seat selected!";
    public static String NO_FLIGHT_SELECTED="No flight selected!";
    public static String REMOVE_FAILED="Booking removal failed!";
    public static String INSERT_FAILED="Booking insert failed!";
    public static String OVERBOOKING(String user,String flight){
        return new StringBuilder().append("There are not enough tickets available for ").append(user)
                .append(" for flight ").append(flight).toString();
    }

}
