package ro.sd.a2.factory;

import ro.sd.a2.entity.Flight;
import ro.sd.a2.entity.FlightBooking;
import ro.sd.a2.entity.User;

import java.util.HashSet;
import java.util.UUID;

public class FlightBookingFactory {

    public static FlightBooking generateFlightBooking(int seats){
        return FlightBooking.builder().user(new User())
                .flight(new Flight())
                .id(UUID.randomUUID().toString())
                .checkedIn(false)
                .nrOfSeats(seats).build();
    }


}
