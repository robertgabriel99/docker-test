package ro.sd.a2.factory;

import ro.sd.a2.entity.Airplane;

import java.util.HashSet;
import java.util.UUID;

public class AirplaneFactory {

    public static Airplane generateAirplane(){
        return Airplane.builder().id(UUID.randomUUID().toString())
                .flights(new HashSet<>()).build();
    }

}
