package ro.sd.a2.factory;

import ro.sd.a2.entity.Airport;

import java.util.HashSet;
import java.util.UUID;

public class AirportFactory {

    public static Airport generateAirport(){
        return Airport.builder().id(UUID.randomUUID().toString())
                .drivers(new HashSet<>())
                .flightsArrival(new HashSet<>())
                .flightsDeparture(new HashSet<>()).build();
    }

}
