package ro.sd.a2.factory;

import ro.sd.a2.entity.Airplane;
import ro.sd.a2.entity.Airport;
import ro.sd.a2.entity.Flight;

import java.util.HashSet;
import java.util.UUID;

public class FlightFactory {

    public static Flight generateFlight(){
        return Flight.builder().id(UUID.randomUUID().toString()).airplane(new Airplane())
                .airportArrival(new Airport()).airportDeparture(new Airport()).flightBookings(new HashSet<>()).build();

    }

}
