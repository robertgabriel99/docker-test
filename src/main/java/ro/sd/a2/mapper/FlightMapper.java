package ro.sd.a2.mapper;

import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.entity.Flight;

public class FlightMapper {

    public static void dtoToEntity(FlightDto flightDto, Flight flight){
        flight.setPrice(flightDto.getPrice());
        flight.setNrOfBookings(flightDto.getNrOfBookings());
        flight.setSeatsAvailable(flightDto.getSeatsAvailable());
        flight.setDepartureTime(flightDto.getDepartureTime());
        flight.setArrivalTime(flightDto.getArrivalTime());
    }

    public static FlightDto entityToDto(Flight flight){
        return FlightDto.builder().airplane(flight.getAirplane().getName())
                .airportArrival(flight.getAirportArrival().getName())
                .airportDeparture(flight.getAirportDeparture().getName())
                .arrivalTime(flight.getArrivalTime())
                .departureTime(flight.getDepartureTime())
                .nrOfBookings(flight.getNrOfBookings())
                .seatsAvailable(flight.getSeatsAvailable())
                .price(flight.getPrice())
                .id(flight.getId()).build();
    }

}
