package ro.sd.a2.mapper;

import ro.sd.a2.dto.FlightBookingDto;
import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.entity.FlightBooking;

public class FlightBookingMapper {

    public static FlightBookingDto entityToDto(FlightBooking flightBooking){
        return FlightBookingDto.builder().checkedIn(flightBooking.isCheckedIn())
                .id(flightBooking.getId())
                .userId(flightBooking.getUser().getId())
                .nrOfSeats(flightBooking.getNrOfSeats())
                .userEmail(flightBooking.getUser().getEmail())
                .flightDto(FlightMapper.entityToDto(flightBooking.getFlight())).build();
    }

}
