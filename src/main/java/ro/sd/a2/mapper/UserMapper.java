package ro.sd.a2.mapper;

import ro.sd.a2.dto.LogUserDto;
import ro.sd.a2.entity.User;

public class UserMapper {

    public static void dtoToEntity(LogUserDto logUserDto,User user){
        user.setEmail(logUserDto.getEmail());
        user.setName(logUserDto.getName());
        user.setPassword(logUserDto.getPassword());
    }

}
