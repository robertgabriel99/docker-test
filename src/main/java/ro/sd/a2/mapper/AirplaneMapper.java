package ro.sd.a2.mapper;

import ro.sd.a2.dto.AirplaneDto;
import ro.sd.a2.entity.Airplane;

public class AirplaneMapper {

    public static void dtoToEntity(AirplaneDto airplaneDto, Airplane airplane){
        airplane.setName(airplaneDto.getName());
        airplane.setCapacity(airplaneDto.getCapacity());
    }

    public static AirplaneDto entityToDto(Airplane airplane){
        return AirplaneDto.builder().id(airplane.getId()).name(airplane.getName())
                .capacity(airplane.getCapacity()).build();
    }

}
