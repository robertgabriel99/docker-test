package ro.sd.a2.mapper;

import ro.sd.a2.dto.AirportDto;
import ro.sd.a2.entity.Airport;

public class AirportMapper {

    public static void dtoToEntity(AirportDto airportDto, Airport airport){
        airport.setName(airportDto.getName());
        airport.setLocation(airportDto.getLocation());
    }

    public static AirportDto entityToDto(Airport airport){
        return AirportDto.builder().name(airport.getName())
                .location(airport.getLocation()).id(airport.getId())
                .location_name(airport.getLocation()+", "+airport.getName()).build();
    }

}
