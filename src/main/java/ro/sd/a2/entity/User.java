package ro.sd.a2.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "user_entity")
public class User implements Serializable {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String email;

    @Column
    private String password;

    @Column
    private Date creationDate;

    @Column
    private Boolean enabled;

    @Column
    private String role;

    @OneToMany(mappedBy = "user",orphanRemoval = true)
    private Set<FlightBooking> flightBookings;

    @OneToMany(mappedBy = "user",orphanRemoval = true)
    private Set<CarRide> carRides;

    public void addFlightBooking(FlightBooking flightBooking){
        flightBookings.add(flightBooking);
        flightBooking.setUser(this);
    }

    public void removeFlightBooking(FlightBooking flightBooking){
        flightBookings.remove(flightBooking);
        flightBooking.setUser(null);
    }

}
