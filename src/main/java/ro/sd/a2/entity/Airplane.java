package ro.sd.a2.entity;


import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "airplanes")
public class Airplane {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private int capacity;

    @OneToMany(mappedBy = "airplane",orphanRemoval = true)
    private Set<Flight> flights;

    public void addFlight(Flight flight){
        flights.add(flight);
        flight.setAirplane(this);
    }

    public void removeFlight(Flight flight){
        flights.remove(flight);
        flight.setAirplane(null);
    }

}
