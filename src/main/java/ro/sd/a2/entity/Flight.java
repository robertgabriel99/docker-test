package ro.sd.a2.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "flights")
public class Flight {

    @Id
    private String id;

    @ManyToOne(fetch=FetchType.LAZY)
    private Airplane airplane;

    @ManyToOne(fetch = FetchType.LAZY)
    private Airport airportDeparture;

    @ManyToOne(fetch = FetchType.LAZY)
    private Airport airportArrival;

    @Column
    private LocalDateTime departureTime;

    @Column
    private LocalDateTime arrivalTime;

    @Column
    private int nrOfBookings;

    @Column
    private int seatsAvailable;

    @Column
    private int price;

    @OneToMany(mappedBy = "flight",orphanRemoval = true)
    private Set<FlightBooking> flightBookings;

    public void addFlightBooking(FlightBooking flightBooking){
        flightBookings.add(flightBooking);
        flightBooking.setFlight(this);
        nrOfBookings=nrOfBookings+ flightBooking.getNrOfSeats();
        seatsAvailable=airplane.getCapacity()-nrOfBookings;
    }

    public void removeFlightBooking(FlightBooking flightBooking){
        flightBookings.remove(flightBooking);
        flightBooking.setFlight(null);
        nrOfBookings=nrOfBookings-flightBooking.getNrOfSeats();
        seatsAvailable=airplane.getCapacity()-nrOfBookings;
    }

}
