package ro.sd.a2.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "airports")
public class Airport {

    @Id
    private String id;

    @Column
    private String name;

    @Column
    private String location;

    @OneToMany(mappedBy = "airportDeparture",orphanRemoval = true)
    private Set<Flight> flightsDeparture;

    @OneToMany(mappedBy = "airportArrival",orphanRemoval = true)
    private Set<Flight> flightsArrival;

    @OneToMany(mappedBy = "airport",orphanRemoval = true)
    private Set<Driver> drivers;

    public void addFlightDeparture(Flight flight){
        flightsDeparture.add(flight);
        flight.setAirportDeparture(this);
    }

    public void removeFlightDeparture(Flight flight){
        flightsDeparture.remove(flight);
        flight.setAirportDeparture(null);
    }

    public void addFlightArrival(Flight flight){
        flightsArrival.add(flight);
        flight.setAirportArrival(this);
    }

    public void removeFlightArrival(Flight flight){
        flightsArrival.remove(flight);
        flight.setAirportArrival(null);
    }


}
