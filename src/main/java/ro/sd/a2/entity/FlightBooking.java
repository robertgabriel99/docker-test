package ro.sd.a2.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "flight_bookings")
public class FlightBooking {

    @Id
    private String id;

    @Column
    private Integer nrOfSeats;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Flight flight;

    @Column
    private boolean checkedIn;


}
