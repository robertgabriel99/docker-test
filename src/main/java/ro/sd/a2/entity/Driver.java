package ro.sd.a2.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "drivers")
public class Driver {

    @Id
    private String id;

    @Column
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    private Airport airport;

    @OneToMany(mappedBy = "driver",orphanRemoval = true)
    private Set<CarRide> carRides;

}
