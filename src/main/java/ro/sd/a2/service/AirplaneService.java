package ro.sd.a2.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.a2.dto.AirplaneDto;
import ro.sd.a2.entity.Airplane;
import ro.sd.a2.exception.AirplaneExceptions;
import ro.sd.a2.factory.AirplaneFactory;
import ro.sd.a2.mapper.AirplaneMapper;
import ro.sd.a2.message.AirplaneMessages;
import ro.sd.a2.service.repository.AirplaneRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AirplaneService {

    private static final Logger log = LoggerFactory.getLogger(AirplaneService.class);

    @Autowired
    private AirplaneRepository airplaneRepository;

    /**
     * functie ce adauga un avion in baza de date
     * @param airplaneDto obiectul primit de la controller
     */
    public void addAirplane(AirplaneDto airplaneDto){
        log.info(AirplaneMessages.INSERT_ATTEMPTED);
        if(StringUtils.isEmpty(airplaneDto.getName())){
            throw new IllegalArgumentException(AirplaneExceptions.EMPTY_NAME);
        }
        if(airplaneDto.getCapacity()==0){
            throw new IllegalArgumentException(AirplaneExceptions.NO_CAPACITY);
        }
        Airplane airplane= AirplaneFactory.generateAirplane();
        AirplaneMapper.dtoToEntity(airplaneDto,airplane);
        airplaneRepository.save(airplane);
        log.info(AirplaneMessages.NEW_AIRPLANE_ADDED(airplaneDto.getName(),airplaneDto.getCapacity()));
    }

    /**
     * functie ce returneaza o lista cu toate avioanele
     * @return lista cu toate avioanele
     */
    public List<AirplaneDto> getAllAirplanes(){
        List<Airplane> airplanes= airplaneRepository.findAll();
        if(airplanes.size()==0){
            throw new RuntimeException(AirplaneExceptions.NO_AIRPLANE_FOUND);
        }
        List<AirplaneDto> airplaneDtos=new ArrayList<>();
        for(Airplane airplane:airplanes){
            airplaneDtos.add(AirplaneMapper.entityToDto(airplane));
        }
        log.info(AirplaneMessages.ALL_AIRPLANES_RETRIEVED);
        return airplaneDtos;
    }

    /**
     * functie ce sterge un avion in functie de nume
     * @param name numele avionului care trebuie sters
     */
    public void removeAirplaneByName(String name){
        log.info(AirplaneMessages.DELETE_ATTEMTED);
        if(StringUtils.isEmpty(name)){
            throw new IllegalArgumentException(AirplaneExceptions.EMPTY_NAME);
        }
        Airplane airplane=airplaneRepository.findAirplaneByName(name);
        if(airplane==null){
            throw new IllegalArgumentException(AirplaneExceptions.NO_AIRPLANE_FOUND);
        }
        log.info(AirplaneMessages.AIRPLANE_DELETED(airplane.getName()));
        airplaneRepository.delete(airplane);
    }

    /**
     * functie ce actualizeaza numele unui avion
     * @param name vechiul nume al avionului
     * @param newName noul nume al avionului
     */
    public void updateAirplaneName(String name,String newName){
        log.info(AirplaneMessages.UPDATE_NAME_ATTEMPTED);
        if(StringUtils.isEmpty(name)||StringUtils.isEmpty(newName)){
            throw new IllegalArgumentException(AirplaneExceptions.EMPTY_NAME);
        }
        Airplane airplane=airplaneRepository.findAirplaneByName(name);
        if(airplane==null){
            throw new RuntimeException(AirplaneExceptions.NO_AIRPLANE_FOUND);
        }
        airplane.setName(newName);
        log.info(AirplaneMessages.UPDATE_SUCCESSFULL(airplane.getName()));
        airplaneRepository.save(airplane);
    }

    /**
     * functie ce actualizeaza capacitatea unui avion
     * @param name numele avionului ce trebuie actualizat
     * @param newCapacity noua capacitate a avionului
     */
    public void updateAirplaneCapacity(String name,int newCapacity){
        log.info(AirplaneMessages.UPDATE_CAPACITY_ATTEMPTED);
        if(StringUtils.isEmpty(name)){
            throw new IllegalArgumentException(AirplaneExceptions.NO_AIRPLANE_FOUND);
        }
        if(newCapacity==0){
            throw new IllegalArgumentException(AirplaneExceptions.NO_CAPACITY);
        }
        Airplane airplane=airplaneRepository.findAirplaneByName(name);
        airplane.setCapacity(newCapacity);
        log.info(AirplaneMessages.UPDATE_SUCCESSFULL(airplane.getName()));
        airplaneRepository.save(airplane);
    }

}
