package ro.sd.a2.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.a2.dto.AirportDto;
import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.dto.UserAndFlight;
import ro.sd.a2.entity.Airplane;
import ro.sd.a2.entity.Airport;
import ro.sd.a2.entity.Flight;
import ro.sd.a2.entity.FlightBooking;
import ro.sd.a2.exception.FlightExceptions;
import ro.sd.a2.factory.FlightFactory;
import ro.sd.a2.mapper.FlightBookingMapper;
import ro.sd.a2.mapper.FlightMapper;
import ro.sd.a2.message.FlightMessages;
import ro.sd.a2.service.repository.AirplaneRepository;
import ro.sd.a2.service.repository.AirportRepository;
import ro.sd.a2.service.repository.FlightRepository;
import ro.sd.a2.utils.MQConfig;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class FlightService {

    private static final Logger log = LoggerFactory.getLogger(FlightService.class);

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private AirplaneRepository airplaneRepository;

    @Autowired
    private AirportRepository airportRepository;

    /**
     * functie ce returneaza toate zborurile
     * @return lista cu toate zborurile
     */
    public List<FlightDto> getAllFlights(){
        List<Flight> flights=flightRepository.findAll();
        List<FlightDto> flightDtos=new ArrayList<>();
        for(Flight flight:flights){
            flightDtos.add(FlightMapper.entityToDto(flight));
        }
        return flightDtos;
    }

    /**
     * functie ce adauga un zbor in baza de date
     * @param flightDto obiectul ce trebuie inserat
     */
    public void addFlight(FlightDto flightDto){
        if(flightDto==null){
            throw new IllegalArgumentException(FlightExceptions.FLIGHT_NULL);
        }
        if(flightDto.getDepartureTime().isAfter(flightDto.getArrivalTime())){
            throw new IllegalArgumentException(FlightExceptions.WRONG_DATES);
        }
        Flight flight= FlightFactory.generateFlight();
        FlightMapper.dtoToEntity(flightDto,flight);
        Airplane airplane=airplaneRepository.findAirplaneByName(flightDto.getAirplane());
        flight.setSeatsAvailable(airplane.getCapacity());
        airportRepository.findAirportByName(flightDto.getAirportArrival()).addFlightArrival(flight);
        airportRepository.findAirportByName(flightDto.getAirportDeparture()).addFlightDeparture(flight);
        airplane.addFlight(flight);
        log.info(FlightMessages.INSERT_SUCCESSFUL(flight.toString()));
        flightRepository.save(flight);
    }

    /**
     * functie ce sterge un zbor din baza de date
     * @param flightDto obiectul ce trebuie sters
     */
    public void removeFlight(FlightDto flightDto){
        if(flightDto==null){
            throw new IllegalArgumentException(FlightExceptions.FLIGHT_NULL);
        }
        Optional<Flight> flight=flightRepository.findById(flightDto.getId());
        if(!flight.isPresent()){
            throw new RuntimeException(FlightExceptions.NO_FLIGHT_FOUND);
        }
        Flight flight1=flight.get();
        flight1.getAirportArrival().removeFlightArrival(flight1);
        flight1.getAirportDeparture().removeFlightDeparture(flight1);
        flight1.getAirplane().removeFlight(flight1);
        log.info(FlightMessages.REMOVE_SUCCESSFUL(flight1.toString()));
        flightRepository.delete(flight1);
    }

    /**
     * functie ce actualizeaza orarul unui zbor
     * @param id id-ul zborului ce trebuie actualizat
     * @param departureTime noua ora de plecare
     * @param arrivalTime noua ora de ajungere
     */
    public void updateFlightTime(String id, LocalDateTime departureTime, LocalDateTime arrivalTime){
        Optional<Flight> flight=flightRepository.findById(id);
        Flight flight1=flight.get();
        flight1.setDepartureTime(departureTime);
        flight1.setArrivalTime(arrivalTime);
        Set<FlightBooking> flightBookingSet= flight1.getFlightBookings();
        for(FlightBooking flightBooking:flightBookingSet){
            template.convertAndSend(MQConfig.EXCHANGE,MQConfig.ROUTING_KEY2, FlightBookingMapper.entityToDto(flightBooking));
        }
        flightRepository.save(flight1);
    }

    /**
     * functie ce actualizeaza avionul asignat unui zbor
     * @param id id-ul zborului ce trebuie actualizat
     * @param airplane numele noului avion asignat zborului
     */
    public void updateFlightAirplane(String id,String airplane){
        if(StringUtils.isEmpty(airplane)){
            throw new IllegalArgumentException(FlightExceptions.NO_AIRPLANE_SELECTED);
        }
        Optional<Flight> flight=flightRepository.findById(id);
        if(!flight.isPresent()){
            throw new RuntimeException(FlightExceptions.NO_FLIGHT_FOUND);
        }
        Flight flight1=flight.get();
        Airplane airplane1=airplaneRepository.findAirplaneByName(airplane);
        flight1.getAirplane().removeFlight(flight1);
        airplane1.addFlight(flight1);
        log.info(FlightMessages.UPDATE_SUCCESSFUL(flight1.toString()));
        flightRepository.save(flight1);
    }

    /**
     * functie ce actualizeaza pretul unui bilet de zbor
     * @param id id-ul zborului ce trebuie actualizat
     * @param newPrice noul pret al biletuluo
     */
    public void updateFlightPrice(String id,int newPrice){
        Optional<Flight> flight=flightRepository.findById(id);
        if(newPrice==0){
            throw new IllegalArgumentException(FlightExceptions.NEW_PRICE_ZERO);
        }
        Flight flight1=flight.get();
        flight1.setPrice(newPrice);
        log.info(FlightMessages.UPDATE_SUCCESSFUL(flight1.toString()));
        flightRepository.save(flight1);
    }

    /**
     * functie ce returneaza zborurile in funtie de locatia de plecare,
     * locatia de destiantie si data
     * @param departure aeroportul de unde are loc plecare
     * @param destination aeroportul destinatie
     * @param date data
     * @return lista cu zborurile
     */
    public List<FlightDto> getFlightsByAirportsAndDate(AirportDto departure,AirportDto destination, LocalDate date){
        Airport airport1=airportRepository.findAirportByName(departure.getName());
        Airport airport2=airportRepository.findAirportByName(destination.getName());
        List<Flight> flights=flightRepository.findAllByAirportDepartureAndAirportArrival(airport1,airport2);
        List<FlightDto> flightDtos=new ArrayList<>();
        for(Flight flight:flights){
            LocalDateTime localDateTime=flight.getDepartureTime();
            if(localDateTime.getYear()==date.getYear()&&localDateTime.getMonth()==date.getMonth()&&localDateTime.getDayOfMonth()==date.getDayOfMonth()){
                flightDtos.add(FlightMapper.entityToDto(flight));
            }
        }
        return flightDtos;
    }

}
