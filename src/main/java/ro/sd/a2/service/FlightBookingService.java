package ro.sd.a2.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.a2.dto.FlightBookingDto;
import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.dto.UserAndFlight;
import ro.sd.a2.entity.Flight;
import ro.sd.a2.entity.FlightBooking;
import ro.sd.a2.entity.User;
import ro.sd.a2.exception.FlightBookingExceptions;
import ro.sd.a2.factory.FlightBookingFactory;
import ro.sd.a2.mapper.FlightBookingMapper;
import ro.sd.a2.message.FlightBookingMessages;
import ro.sd.a2.service.repository.FlightBookingRepository;
import ro.sd.a2.service.repository.FlightRepository;
import ro.sd.a2.service.repository.UserRepository;
import ro.sd.a2.utils.MQConfig;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class FlightBookingService {

    private static final Logger log = LoggerFactory.getLogger(FlightBookingService.class);

    @Autowired
    private RabbitTemplate template;

    @Autowired
    private FlightBookingRepository flightBookingRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FlightRepository flightRepository;

    /**
     * functie ce adauga un booking de zbor in baza de date
     * @param userId id-ul user-ului care face bookingul
     * @param flightDto obiectul zbor trimit de controller
     * @param nrOfSeats numarul de scaune rezervate
     */
    public void addFlightBooking(String userId, FlightDto flightDto,int nrOfSeats){
        if(flightDto==null){
            throw new IllegalArgumentException(FlightBookingExceptions.NO_FLIGHT_SELECTED);
        }
        if(nrOfSeats==0){
            throw new IllegalArgumentException(FlightBookingExceptions.NO_SEAT_SELECTED);
        }
        FlightBooking flightBooking= FlightBookingFactory.generateFlightBooking(nrOfSeats);
        User user=userRepository.findById(userId).get();
        Flight flight=flightRepository.findById(flightDto.getId()).get();
        if(nrOfSeats>flight.getAirplane().getCapacity()-flight.getNrOfBookings()){
            throw new IllegalArgumentException(FlightBookingExceptions.OVERBOOKING(user.getEmail(),flight.toString()));
        }
        user.addFlightBooking(flightBooking);
        flight.addFlightBooking(flightBooking);
        log.info(FlightBookingMessages.BOOKING_SUCCESSFUL(user.getEmail(),flight.toString()));
        template.convertAndSend(MQConfig.EXCHANGE,MQConfig.ROUTING_KEY1,UserAndFlight.builder().email(user.getEmail()).flightDto(flightDto).nrOfTickets(nrOfSeats).build());
        flightBookingRepository.save(flightBooking);
    }

    /**
     * functie ce returneaza bookingurile de zbor inca valide in functie de un user
     * @param userId id-ul user-ului pentru care se returneaza zborurile
     * @return lista cu bookingurile active pentru userul cu id-ul parametru
     */
    public List<FlightBookingDto> getActiveBookingsByUser(String userId){
        User user=userRepository.findById(userId).get();
        List<FlightBooking> flightBookings=flightBookingRepository.findAllByUser(user);
        List<FlightBookingDto> flightBookingDtos=new ArrayList<>();
        for(FlightBooking flightBooking:flightBookings){
            if(flightBooking.getFlight().getDepartureTime().isAfter( LocalDateTime.now())){
                flightBookingDtos.add(FlightBookingMapper.entityToDto(flightBooking));
            }
        }
        return flightBookingDtos;
    }

    /**
     * functie ce returneaza bookingurile de zbor din trecut in functie de un user
     * @param userId id-ul user-ului pentru care se returneaza zborurile
     * @return lista cu istoricul de bookinguri pentru userul cu id-ul parametru
     */
    public List<FlightBookingDto> getHistoryOfBookingsByUser(String userId){
        User user=userRepository.findById(userId).get();
        List<FlightBooking> flightBookings=flightBookingRepository.findAllByUser(user);
        List<FlightBookingDto> flightBookingDtos=new ArrayList<>();
        for(FlightBooking flightBooking:flightBookings){
            if(flightBooking.getFlight().getDepartureTime().isBefore( LocalDateTime.now())){
                flightBookingDtos.add(FlightBookingMapper.entityToDto(flightBooking));
            }
        }
        return flightBookingDtos;
    }

    /**
     * functie ce sterge un booking de zbor din baza de date
     * @param flightBookingDto obiectul ce trebuie sters
     */
    public void removeBooking(FlightBookingDto flightBookingDto){
        if(flightBookingDto==null){
            throw new IllegalArgumentException(FlightBookingExceptions.NO_FLIGHT_SELECTED);
        }
        FlightBooking flightBooking=flightBookingRepository.findById(flightBookingDto.getId()).get();
        User user=userRepository.findById(flightBookingDto.getUserId()).get();
        Flight flight=flightRepository.findById(flightBookingDto.getFlightDto().getId()).get();
        user.removeFlightBooking(flightBooking);
        flight.removeFlightBooking(flightBooking);
        flightBookingRepository.delete(flightBooking);
    }

}
