package ro.sd.a2.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.a2.dto.LogUserDto;
import ro.sd.a2.dto.UserDto;
import ro.sd.a2.entity.User;
import ro.sd.a2.exception.LoginExceptions;
import ro.sd.a2.exception.SignUpExceptions;
import ro.sd.a2.factory.UserFactory;
import ro.sd.a2.factory.UserRoleEnum;
import ro.sd.a2.mapper.UserMapper;
import ro.sd.a2.message.LoginMessages;
import ro.sd.a2.message.SignUpMessages;
import ro.sd.a2.service.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    private NameService nameService;

    public UserService(NameService nameService){
        this.nameService=nameService;
    }

    /**
     * functie ce adauga un user in baza de date
     * @param logUserDto userul ce trebuie inserat
     */
    public void signUpUser(LogUserDto logUserDto){
        log.info(SignUpMessages.SIGN_UP_ATTEMPTED);
        if(StringUtils.isEmpty(logUserDto.getEmail())){
            throw new IllegalArgumentException(SignUpExceptions.EMAIL_EMPTY);
        }
        if(!StringUtils.contains(logUserDto.getEmail(),"@")){
            throw new IllegalArgumentException(SignUpExceptions.INVALID_EMAIL_FORMAT(logUserDto.getEmail()));
        }
        if(StringUtils.isEmpty(logUserDto.getName())){
            throw new IllegalArgumentException(SignUpExceptions.NAME_EMPTY);
        }
        if(StringUtils.isEmpty(logUserDto.getPassword())){
            throw new IllegalArgumentException(SignUpExceptions.PASSWORD_EMPTY);
        }
        if(logUserDto.getPassword().length()<6){
            throw new IllegalArgumentException(SignUpExceptions.PASSWORD_TOO_SHORT);
        }
        User user=UserFactory.generateUserWithRole(UserRoleEnum.REGULAR);
        UserMapper.dtoToEntity(logUserDto,user);
        log.info(SignUpMessages.SIGN_UP_SUCCESSFUL(logUserDto.getEmail()));
        userRepository.save(user);
    }

    /**
     * functie ce returneaza un id-ul unui user care se logheaza
     * @param logUserDto userul care se logheaza
     * @return id-ul userului
     */
    public String logInUser(LogUserDto logUserDto){
        log.info(LoginMessages.LOGIN_ATTEMPTED);
        if(StringUtils.isEmpty(logUserDto.getEmail())){
            throw new IllegalArgumentException(LoginExceptions.noEmailInserted);
        }
        if(StringUtils.isEmpty(logUserDto.getPassword())){
            throw new IllegalArgumentException(LoginExceptions.noPasswordInserted);
        }
        User user=userRepository.findByEmail(logUserDto.getEmail());
        if(user==null){
            throw new IllegalArgumentException(LoginExceptions.invalidEmailLogin(logUserDto.getEmail()));
        }
        if(!StringUtils.equals(logUserDto.getPassword(),user.getPassword())){
            throw new IllegalArgumentException(LoginExceptions.invalidPassword);
        }
        log.info(LoginMessages.LOGIN_SUCCESSFULL(user.getEmail()));
        return user.getId();
    }

    public String getByName(String name){
        return nameService.getUserName(name);
    }

    public String greet(){
        return "hello";
    }

}
