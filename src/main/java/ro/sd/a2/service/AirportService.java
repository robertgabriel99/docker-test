package ro.sd.a2.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.sd.a2.dto.AirportDto;
import ro.sd.a2.entity.Airport;
import ro.sd.a2.exception.AirportExceptions;
import ro.sd.a2.factory.AirportFactory;
import ro.sd.a2.mapper.AirportMapper;
import ro.sd.a2.message.AirportMessages;
import ro.sd.a2.service.repository.AirportRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class AirportService {

    private static final Logger log = LoggerFactory.getLogger(AirportService.class);

    @Autowired
    private AirportRepository airportRepository;

    /**
     * functie ce adauga un aeroport in baza de date
     * @param airportDto obiectul primit de la controller
     */
    public void addAirport(AirportDto airportDto){
        log.info(AirportMessages.INSERT_ATTEMPTED);
        if(airportDto==null){
            throw new IllegalArgumentException(AirportExceptions.NULL_OBJECT_INSERT);
        }
        Airport airport= AirportFactory.generateAirport();
        AirportMapper.dtoToEntity(airportDto,airport);
        log.info(AirportMessages.INSERT_SUCCESSFUL(airport.getName()));
        airportRepository.save(airport);
    }

    /**
     * functie ce returneaza o lista cu toate aeroporturile
     * @return lista cu toate aeroporturile
     */
    public List<AirportDto> getAllAirports(){
        List<Airport> airports=airportRepository.findAll();
        if(airports.size()==0){
            log.info(AirportExceptions.NO_AIRPORTS_RETRIEVED);
        }
        List<AirportDto> airportDtos=new ArrayList<>();
        for(Airport airport:airports){
            airportDtos.add(AirportMapper.entityToDto(airport));
        }
        log.info(AirportMessages.ALL_AIRPORTS_RETRIEVED);
        return airportDtos;
    }

    /**
     * functie ce sterge un aeroport din baza de date
     * @param name numele aeroportului ce trebuie sters
     */
    public void removeAirportByName(String name){
        if(StringUtils.isEmpty(name)){
            throw new IllegalArgumentException(AirportExceptions.EMPTY_NAME);
        }
        Airport airport=airportRepository.findAirportByName(name);
        if(airport==null){
            throw new RuntimeException(AirportExceptions.NO_AIRPORTS_RETRIEVED);
        }
        log.info(AirportMessages.REMOVE_SUCCESSFULL(airport.getName()));
        airportRepository.delete(airport);
    }

    /**
     * functie ce actualizeaza numele unui aerport
     * @param oldAirportName vechiul nume al aeroportului
     * @param newAirportName noul nume al aeroportului
     */
    public void updateAirportName(String oldAirportName, String newAirportName){
        log.info(AirportMessages.UPDATE_ATTEMPTED);
        if(StringUtils.isEmpty(oldAirportName)||StringUtils.isEmpty(newAirportName)){
            throw new IllegalArgumentException(AirportExceptions.EMPTY_NAME);
        }
        Airport airport=airportRepository.findAirportByName(oldAirportName);
        if(airport==null){
            throw new RuntimeException(AirportExceptions.NO_AIRPORTS_RETRIEVED);
        }
        airport.setName(newAirportName);
        log.info(AirportMessages.UPDATE_SUCCESSFUL);
        airportRepository.save(airport);
    }


}
