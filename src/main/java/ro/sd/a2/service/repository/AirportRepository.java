package ro.sd.a2.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.sd.a2.entity.Airport;


@Repository
public interface AirportRepository extends JpaRepository<Airport,String> {

    Airport findAirportByName(String name);


}
