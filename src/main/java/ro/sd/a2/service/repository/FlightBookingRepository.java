package ro.sd.a2.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ro.sd.a2.entity.FlightBooking;
import ro.sd.a2.entity.User;

import java.util.List;

@Repository
public interface FlightBookingRepository extends JpaRepository<FlightBooking,String> {

    List<FlightBooking> findAllByUser(User user);

}
