package ro.sd.a2.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FlightBookingDto {

    private String id;
    private String userEmail;
    private String userId;
    private FlightDto flightDto;
    private boolean checkedIn;
    private Integer nrOfSeats;

}
