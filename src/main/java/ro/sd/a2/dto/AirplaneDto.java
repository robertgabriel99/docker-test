package ro.sd.a2.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AirplaneDto {

    private String id;
    private String name;
    private int capacity;

}
