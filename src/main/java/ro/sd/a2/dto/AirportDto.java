package ro.sd.a2.dto;

import lombok.*;

import javax.persistence.Entity;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AirportDto {

    private String id;
    private String name;
    private String location;
    private String location_name;

}
