package ro.sd.a2.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class FlightDto {

    private String id;

    private String airplane;

    private String airportDeparture;

    private String airportArrival;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime departureTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime arrivalTime;

    private int nrOfBookings;

    private int seatsAvailable;

    private int price;

    public String toString(){
        return new StringBuilder().append(airportDeparture).append(" ")
                .append(airportArrival).append(" ").append(airplane).append(" ")
                .append(departureTime).append(" ").append(arrivalTime).append(" ")
                .append(nrOfBookings).append(" ").append(price).toString();
    }

}
