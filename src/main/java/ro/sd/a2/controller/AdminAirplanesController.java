
package ro.sd.a2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.dto.AirplaneDto;
import ro.sd.a2.exception.AirplaneExceptions;
import ro.sd.a2.service.AirplaneService;

import javax.validation.Valid;

@Controller
public class AdminAirplanesController {

    private static final Logger log = LoggerFactory.getLogger(AdminAirplanesController.class);

    @Autowired
    private AirplaneService airplaneService;

    /**
     * functie ce returneaza pagina de administrare a avioanelor
     */
    @GetMapping("/admin_airplanes")
    public ModelAndView page(){
        ModelAndView modelAndView=new ModelAndView();
        AirplaneDto airplaneDto=new AirplaneDto();
        String newAirplaneName="";
        int newCapacity=0;
        modelAndView.addObject("airplaneDto",airplaneDto);
        modelAndView.addObject("airplanes",airplaneService.getAllAirplanes());
        modelAndView.addObject("newAirplaneName",newAirplaneName);
        modelAndView.addObject("newCapacity",newCapacity);
        modelAndView.setViewName("admin_airplanes");
        return modelAndView;
    }

    /**
     * functie ce adauga un avion in baza de date si returneaza pagina
     * @param airplane obiectul extras din view
     * @return pagina
     */
    @PostMapping(value = "/admin_airplanes/add_airplane")
    public ModelAndView addAirplane(@ModelAttribute("airplaneDto") AirplaneDto airplane){
        try{
            airplaneService.addAirplane(airplane);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return page();
    }

    /**
     * functie ce sterge un avion din baza de date
     * @param airplaneToDelete numele avionului ce trebui sters, extras din view
     * @return pagina
     */
    @PostMapping(value = "/admin_airplanes/delete_airplane")
    public ModelAndView removeAirplane(@Valid String airplaneToDelete){
        try{
            airplaneService.removeAirplaneByName(airplaneToDelete);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return page();
    }

    /**
     * functie ce actualizeaza numele unui avion
     * @param airplaneToUpdateName numele avionului care trebuie actualizat
     * @param newAirplaneName noul nume al avionului
     * @return pagina
     */
    @PostMapping(value = "/admin_airplanes/update_airplane_name")
    public ModelAndView updateAirplaneName(@Valid String airplaneToUpdateName,
                                           @RequestParam(value = "newAirplaneName",required = false) String newAirplaneName){
        try{
            airplaneService.updateAirplaneName(airplaneToUpdateName,newAirplaneName);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return page();
    }

    /**
     * functie ce actualizeaza capacatitatea unui avion
     * @param airplaneToUpdateCapacity numele avionului ce trebuie actualizat
     * @param newCapacity noua capacitate a avoinului
     * @return pagina
     */
    @PostMapping(value = "/admin_airplanes/update_airplane_capacity")
    public ModelAndView updateAirplaneCapacity(@Valid String airplaneToUpdateCapacity,
                                               @RequestParam(value = "newCapacity",required = false) int newCapacity){
        try{
            airplaneService.updateAirplaneCapacity(airplaneToUpdateCapacity,newCapacity);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return page();
    }

    @GetMapping(value = "/robert")
    public void f(){
        System.out.println("afasd");
    }

}
