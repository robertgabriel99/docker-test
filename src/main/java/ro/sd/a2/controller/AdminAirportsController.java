package ro.sd.a2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.dto.AirportDto;
import ro.sd.a2.exception.AirportExceptions;
import ro.sd.a2.service.AirportService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;

@Controller
public class AdminAirportsController {

    private static final Logger log = LoggerFactory.getLogger(AdminAirplanesController.class);

    @Autowired
    private AirportService airportService;

    HashMap<String,AirportDto> map;

    /**
     * metoda ce returneaza pagina de administrare a aeroporturilor
     */
    @GetMapping("/admin_airports")
    public ModelAndView page(){
        map=new HashMap<>();
        ModelAndView modelAndView=new ModelAndView();
        AirportDto airportDto=new AirportDto();
        List<AirportDto> airportDtoList= airportService.getAllAirports();
        for(AirportDto airportDto1:airportDtoList){
            map.put(airportDto1.getName(),airportDto1);
        }
        modelAndView.addObject("airportDto",airportDto);
        modelAndView.addObject("airports",airportDtoList);
        modelAndView.setViewName("admin_airports");
        return modelAndView;
    }

    /**
     * functie ce adauga un aeroport in baza de date
     * @param airportDto obiectul ce trebuie inserat
     * @return pagina
     */
    @PostMapping(value = "/admin_airports/add_airport")
    public ModelAndView addAirport(@ModelAttribute("airportDto") AirportDto airportDto){
        try{
            airportService.addAirport(airportDto);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(AirportExceptions.INSERT_FAILED);
        }
        return page();
    }

    /**
     * functie ce sterge un aeroport din baza de date
     * @param airportToDelete numele aeroportului ce trebuie sters
     * @return pagina
     */
    @PostMapping(value = "/admin_airports/delete_airport")
    public ModelAndView removeAirport(@Valid String airportToDelete){
        try{
            airportService.removeAirportByName(airportToDelete);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return page();
    }

    /**
     * functie ce actualizeaza numele unui aeroport
     * @param airportToUpdate numele aeroportului ce trebuie actualizat
     * @param newAirportName noul nume al aeroportului
     * @return pagina
     */
    @PostMapping(value = "/admin_airports/update_airport/update_name")
    public ModelAndView updateAirport(@Valid String airportToUpdate,
                                      @RequestParam(value = "newAirportName",required = false) String newAirportName){
        try{
            airportService.updateAirportName(airportToUpdate,newAirportName);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(AirportExceptions.UPDATE_FAILED);
        }
        return page();
    }


}
