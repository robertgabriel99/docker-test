package ro.sd.a2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.dto.AirportDto;
import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.exception.FlightBookingExceptions;
import ro.sd.a2.exception.FlightExceptions;
import ro.sd.a2.service.AirportService;
import ro.sd.a2.service.FlightBookingService;
import ro.sd.a2.service.FlightService;
import ro.sd.a2.utils.Counter;

import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@RestController
public class BookFlightController {

    private static final Logger log = LoggerFactory.getLogger(BookFlightController.class);

    @Autowired
    private FlightBookingService flightBookingService;

    @Autowired
    private AirportService airportService;

    @Autowired
    private FlightService flightService;


    private HashMap<String, AirportDto> airports;

    private List<FlightDto> flightDtoList;

    private int nrOfSeats;

    /**
     * functie ce returneaza pagina pentru realizarea unui booking de catre un utlizator
     * @param userId id-ul user-ului care s-a logat
     * @return pagina
     */
    @GetMapping("/book_flight/{userId}")
    public ModelAndView mainPage(@PathVariable String userId) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("userId",userId);
        mav.addObject("flag",false);
        mav.addObject("flagOverbook",false);
        List<AirportDto> airportDtoList = airportService.getAllAirports();
        airports=new HashMap<>();
        for(AirportDto airportDto: airportDtoList){
            airports.put(airportDto.getLocation_name(),airportDto);
        }
        mav.addObject("counter",new Counter());
        mav.addObject("airports", airportDtoList);
        mav.setViewName("book_flight");
        return mav;
    }

    /**
     * functie ce actualizeaza pagina, in urma cautarii unui anumit zbor de catre user
     * @param departure locatia de plecare cautata
     * @param destination locatia de destinatie cautata
     * @param userId id-ul user-ului
     * @param departureTime data de plecare cautata
     * @param nrOfSeats numarul de scaune dorite
     * @return pagina actualizata
     */
    @PostMapping(value = "/book_flight/{userId}",params = "action=Search")
    public ModelAndView searchFlights(@Valid String departure, @Valid String destination,@PathVariable String userId,
                                      @RequestParam(value = "departureTime") String departureTime,
                                      @RequestParam(value = "seats") int nrOfSeats){
        ModelAndView mav=mainPage(userId);
        this.nrOfSeats=nrOfSeats;
        flightDtoList=flightService.getFlightsByAirportsAndDate(
                airports.get(departure),airports.get(destination),
                LocalDate.parse(departureTime, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        mav.addObject("flights",flightDtoList);
        if(flightDtoList.size()==0){
            mav.addObject("flag",true);
            log.error(FlightExceptions.NO_FLIGHT_FOUND);
        }
        return mav;
    }

    /**
     * functie ce realizeaza un booking, in urma apasarii butonului din dreptul zborului
     * pentru care doreste realizarea unui booking
     * @param mycounter numarul optiunii din zborurile returnate de functia de search
     * @param userId id-ul user-ului
     * @return pagina actualizata
     */
    @PostMapping(value = "book_flight/{userId}/{mycounter}")
    public ModelAndView book(@PathVariable int mycounter,@PathVariable String userId){
        ModelAndView mav=mainPage(userId);
        mav.addObject("flights",flightDtoList);
        try{
            flightBookingService.addFlightBooking(userId,flightDtoList.get(mycounter-1),nrOfSeats);

        }catch (Exception e){
            log.error(e.getMessage());
            if(e.getMessage().contains("There are not enough tickets available for ")){
                mav.addObject("flagOverbook",true);
            }
            log.error(FlightBookingExceptions.INSERT_FAILED);
        }
        return mav;
    }

}
