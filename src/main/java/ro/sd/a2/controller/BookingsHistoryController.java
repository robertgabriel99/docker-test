package ro.sd.a2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.dto.FlightBookingDto;
import ro.sd.a2.service.FlightBookingService;
import ro.sd.a2.utils.Counter;

import java.util.List;

@Controller
public class BookingsHistoryController {

    @Autowired
    private FlightBookingService flightBookingService;

    List<FlightBookingDto> activeBookings;
    List<FlightBookingDto> historyOfBookings;

    /**
     * functie ce returneaza o pagina cu toate rezervarile de zbor efectuate de user
     * @param userId id-ul userului
     * @return pagina
     */
    @GetMapping("/my_bookings/{userId}")
    public ModelAndView bookingsPage(@PathVariable String userId){
        ModelAndView modelAndView=new ModelAndView();
        activeBookings= flightBookingService.getActiveBookingsByUser(userId);
        historyOfBookings=flightBookingService.getHistoryOfBookingsByUser(userId);
        modelAndView.addObject("counter",new Counter());
        modelAndView.addObject("activeBookings",activeBookings);
        modelAndView.addObject("historyOfBookings",historyOfBookings);
        modelAndView.setViewName("my_bookings");
        return modelAndView;
    }

    /**
     * functie ce sterge un booking pentru un zbor
     * @param userId id-ul userului
     * @param index numarul optiunii alese din lista de booking-uri
     * @return pagina actualizata
     */
    @PostMapping("/my_bookings/{userId}/remove_booking/{index}")
    public ModelAndView removeBooking(@PathVariable String userId, @PathVariable int index){
        flightBookingService.removeBooking(activeBookings.get(index-1));
        return bookingsPage(userId);
    }

}
