package ro.sd.a2.controller;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ro.sd.a2.dto.LogUserDto;
import ro.sd.a2.entity.User;
import ro.sd.a2.service.UserService;


@Controller
public class LoginController {

    private static final Logger log = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private UserService userService;

    /**
     * functie ce returneaza pagina de login
     */
    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView mav = new ModelAndView();
        LogUserDto logUserDto=new LogUserDto();
        mav.addObject("logUserDto",logUserDto);
        mav.setViewName("login");
        return mav;
    }

    /**
     * functie ce redirecteaza pagina de login spre pagina de sign up
     * @return pagina de sign up
     */
    @PostMapping(value = "/login",params = "action=signup")
    public RedirectView switchToSignUp(){
        RedirectView redirectView=new RedirectView();
        redirectView.setUrl("signUp");
        return redirectView;
    }

    /**
     * functie ce realizeaza logarea user-ului si redirectare spre pagina de realizarea a unei rezervari
     * in caz de succes, sau de ramanere pe pagina in caz ca apara erori
     * @param logUserDto dto-ul user-ului ce trebuie sa se logheze
     * @return noua pagina
     */
    @PostMapping(value = "/login",params = "action=login")
    public RedirectView switchToMainPage(@ModelAttribute("logUserDto") LogUserDto logUserDto){
        RedirectView redirectView=new RedirectView();
        try{
            if(StringUtils.equals(logUserDto.getEmail(),"admin@admin.com")&&
            StringUtils.equals(logUserDto.getPassword(),"admin")){
                redirectView.setUrl("admin_home");
            }
            else{
                String id=userService.logInUser(logUserDto);
                redirectView.setUrl("book_flight/"+id);
            }
        }catch (Exception e){
            log.error(e.getMessage());
            redirectView.setUrl("login");
        }
        return redirectView;
    }


}
