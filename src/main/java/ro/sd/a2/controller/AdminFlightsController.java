package ro.sd.a2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.dto.AirplaneDto;
import ro.sd.a2.dto.FlightDto;
import ro.sd.a2.exception.FlightExceptions;
import ro.sd.a2.service.AirplaneService;
import ro.sd.a2.service.AirportService;
import ro.sd.a2.service.FlightService;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

@Controller
public class AdminFlightsController {

    private static final Logger log = LoggerFactory.getLogger(AdminFlightsController.class);

    @Autowired
    private FlightService flightService;

    @Autowired
    private AirportService airportService;

    @Autowired
    private AirplaneService airplaneService;

    HashMap<String,FlightDto> map;

    /**
     * functie ce returneaza pagina de administrare a zborurilor
     */
    @GetMapping(value = "/admin_flights")
    public ModelAndView page(){
        ModelAndView modelAndView=new ModelAndView();
        map=new HashMap<>();
        List<FlightDto> flightDtoList=flightService.getAllFlights();
        for(FlightDto flightDto:flightDtoList){
            map.put(flightDto.toString(),flightDto);
        }
        FlightDto flightDto=new FlightDto();
        modelAndView.addObject("flag",false);
        modelAndView.addObject("flag1",false);
        modelAndView.addObject("flights",flightDtoList);
        modelAndView.addObject("flightDto",flightDto);
        modelAndView.addObject("airports",airportService.getAllAirports());
        modelAndView.addObject("airplanes",airplaneService.getAllAirplanes());
        modelAndView.setViewName("admin_flights");
        return modelAndView;
    }

    /**
     * functie ce adauga un zbor in baza de date
     * @param flightDto obiectul ce trebuie adaugat
     * @return pagina
     */
    @PostMapping(value = "/admin_flights/add_flight")
    public ModelAndView addFlight(@ModelAttribute("flightDto") FlightDto flightDto){
        ModelAndView modelAndView=page();
        try{
            flightService.addFlight(flightDto);
        }catch (Exception e){
            log.error(e.getMessage());
            if(e.getMessage().equals(FlightExceptions.WRONG_DATES)){
                modelAndView.addObject("flag",true);
            }
            log.error(FlightExceptions.INSERT_FAILED);
        }
        return modelAndView;
    }

    /**
     * functie ce sterge un zbor din baza de date
     * @param flightToDelete zborul ce trebuie sters, sub forma de String
     * @return pagina
     */
    @PostMapping(value = "/admin_flights/delete_flight")
    public ModelAndView removeFlight(@Valid String flightToDelete){
        try{
            flightService.removeFlight(map.get(flightToDelete));
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(FlightExceptions.REMOVE_FAILED);
        }
        return page();
    }

    /**
     * functie ce actualizeaza orarul unui zbor
     * @param flightToUpdateTime formatul String al zborului
     * @param newDepartureTime noua data de plecare
     * @param newArrivalTime noua data de ajungere
     * @return pagina
     */
    @PostMapping(value = "/admin_flights/update_flight/flight_time")
    public ModelAndView updateFlightTime(@Valid String flightToUpdateTime,
                                         @RequestParam(value="newDepartureTime")String newDepartureTime,
                                         @RequestParam(value = "newArrivalTime")String newArrivalTime){
        ModelAndView modelAndView=page();
        try{
            flightService.updateFlightTime(map.get(flightToUpdateTime).getId(),LocalDateTime.parse(newDepartureTime, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")),
                    LocalDateTime.parse(newArrivalTime,DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm")));

        }catch (Exception e){
            log.error(e.getMessage());
            if(e.getMessage().equals(FlightExceptions.WRONG_DATES)){
                modelAndView.addObject("flag1",true);
            }
            log.error(FlightExceptions.UPDATE_FAILED);
        }
        return modelAndView;
    }

    /**
     * functie ce actualizeaza avionul asignat unui zbor
     * @param flightToUpdateAirplane formatul String al zborului
     * @param newFlightAirplane numele noului avion asignat zborului
     * @return pagina
     */
    @PostMapping(value = "/admin_flights/update_flight/airplane")
    public ModelAndView updateFlightAirplane(@Valid String flightToUpdateAirplane,
                                             @Valid String newFlightAirplane){
        try{
            flightService.updateFlightAirplane(map.get(flightToUpdateAirplane).getId(),newFlightAirplane);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(FlightExceptions.UPDATE_FAILED);
        }
        return page();
    }

    /**
     * functie ce actualizeaza pretul unui zbor
     * @param newPrice noul pret al zborului
     * @param flightToUpdatePrice zborul ce trebuie actualizat,sub forma de String
     * @return pagina
     */
    @PostMapping(value = "/admin_flights/update_flight/price")
    public ModelAndView updateFlightPrice(@RequestParam(value = "newPrice") int newPrice,
                                          @Valid String flightToUpdatePrice){
        try{
            flightService.updateFlightPrice(map.get(flightToUpdatePrice).getId(),newPrice);
        }catch (Exception e){
            log.error(e.getMessage());
            log.error(FlightExceptions.UPDATE_FAILED);
        }
        return page();
    }

}
