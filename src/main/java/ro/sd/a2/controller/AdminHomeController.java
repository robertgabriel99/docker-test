package ro.sd.a2.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ro.sd.a2.service.UserService;

@RestController
public class AdminHomeController {

    @Autowired
    private UserService userService;

    @GetMapping("/admin_home")
    public ModelAndView modelAndView(){
        ModelAndView modelAndView=new ModelAndView();
        modelAndView.setViewName("admin_home");
        return modelAndView;
    }

    @RequestMapping("/home")
    public @ResponseBody String greeting(){
        return userService.greet();
    }


}
