package ro.sd.a2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import ro.sd.a2.dto.LogUserDto;
import ro.sd.a2.service.UserService;

import java.net.URI;

@RestController
public class SignUpController {

    private static final Logger log = LoggerFactory.getLogger(SignUpController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private RestTemplate restTemplate;

    private final String token="f5bd61a2-b875-11eb-8529-0242ac13000310f781b4-b876-11eb-8529-0242ac130003";

    /**
     * functie ce returneaza pagina de sign up
     */
    @GetMapping("/signUp")
    public ModelAndView signUpPage(){
        ModelAndView mav = new ModelAndView();
        LogUserDto logUserDto=new LogUserDto();
        mav.addObject("logUserDto",logUserDto);
        mav.setViewName("signup");
        return mav;
    }


    /**
     * functie ce adauga un user in baza de date
     * @param logUserDto noul user ce trebuie adaugat
     * @return pagina de login in caz de succes sau pagina curenta in caz ca apar erori
     */
    @PostMapping(value = "/signUp",params = "action=register")
    public RedirectView signUp(@ModelAttribute("logUserDto") LogUserDto logUserDto){
        RedirectView redirectView=new RedirectView();
        try{
            userService.signUpUser(logUserDto);
            String url="http://localhost:7798/app/sendEmail/";
            URI uri=new URI(url);
            HttpHeaders headers=new HttpHeaders();
            headers.set("auth",token);
            HttpEntity<LogUserDto> request=new HttpEntity<>(logUserDto,headers);
            ResponseEntity<String> result=restTemplate.postForEntity(uri,request,String.class);
            log.info(String.valueOf(result.getStatusCodeValue()));
            redirectView.setUrl("login");
        }catch (Exception e){
            log.error(e.getMessage());
            redirectView.setUrl("signUp");
        }
        return redirectView;
    }

    /**
     * functie ce asigura intoarcerea la pagina de login
     * @return pagina de login
     */
    @PostMapping(value = "/signUp",params = "action=login")
    public RedirectView switchToLogin(){
        RedirectView redirectView=new RedirectView();
        redirectView.setUrl("login");
        return redirectView;
    }


}
